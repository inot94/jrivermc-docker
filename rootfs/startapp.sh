#!/bin/sh
export HOME=/config

echo '--------------------------------------------------'
echo 'Unofficial JRiver MC Image.'
echo "Image Version: $(cat /VERSION)"
echo ''
echo 'Source: https://gitlab.shio.at/max/jrivermc-docker'
echo 'Dockerhub: https://hub.docker.com/u/shiomax'
echo ''
echo '--------------------------------------------------'

exec env HOME=/config mediacenter%%MC_VERSION%% /mediaserver
