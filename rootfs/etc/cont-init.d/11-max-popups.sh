#!/usr/bin/with-contenv sh

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

log() {
    echo "[cont-init.d] $(basename $0): $*"
}

if [ "$MAXIMIZE_POPUPS" = "1" ]; then
    log "Applying max popup config to openbox."
    sed -i '/<\/applications>/{
        r /templates/max-popups.xml
        a \</applications>
        d
    }' /etc/xdg/openbox/rc.xml
else
    log "Popups won't be maximized"
fi;