#!/bin/bash

echo "removing containers from $DOCKER_REPO locally"

CONTAINERS_TO_REMOVE=$(docker ps -a | awk -v repo_match="^$DOCKER_REPO:*" '{if ($2 ~ repo_match) { print $1 }}')

if [ -n "$CONTAINERS_TO_REMOVE" ]; then
    docker rm -f $CONTAINERS_TO_REMOVE
fi

echo "removing images from $DOCKER_REPO locally"

IMAGES_TO_REMOVE=$(docker images "$DOCKER_REPO" | awk '{if ($3 != "IMAGE") { print $3 }}')

if [ -n "$IMAGES_TO_REMOVE" ]; then
    docker rmi -f $IMAGES_TO_REMOVE
fi