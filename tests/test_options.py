#! /usr/bin/python3

import unittest
from time import sleep
from test_base import TestBase

class TestOptions(unittest.TestCase, TestBase):

    def setUp(self):
        super(TestOptions, self).setupCommon()

    def tearDown(self):
        super(TestOptions, self).tearDownContainer()

    def test_maximize_popups(self):
        super(TestOptions, self).setUpContainer(dockerExtraOpts = ["-e", "MAXIMIZE_POPUPS=1"])

        while self.timeout > 0:
            try:
                cp = super(TestOptions, self).dockerExec("cat /etc/xdg/openbox/rc.xml")
                if cp.returncode == 0 and '<application type="normal" name="JRiver Popup Class">' in cp.stdout:
                    break
            finally:
                self.timeout-=1
                sleep(1)

        self.assertFalse(self.timeout == 0)


