# JRiver Media Center - Docker

This is a docker image for [JRiver Media Center](https://jriver.com/). Images are based off [jlesage/baseimage-gui](https://github.com/jlesage/docker-baseimage-gui). But might also be based off [shiomax/docker-baseimage-gui](https://gitlab.shio.at/max/docker-baseimage-gui) at times.

The sourcecode is available at [gitlab.shio.at](https://gitlab.shio.at/max/jrivermc-docker).

It's intended for use as a central 'headless' JRiver server you stream music from with any of your other JRiver instances.

Direct music playback from the container (can) be setup, but is dependent on having alsa running on the host (see Sound Section).

# Dockerhub

| JRiver Version | Repository |
|----------------|------------|
| Media Center 26 | [shiomax/jrivermc26](https://hub.docker.com/r/shiomax/jrivermc26) |
| Media Center 25 | [shiomax/jrivermc25](https://hub.docker.com/r/shiomax/jrivermc25) |
| Media Center 24 | [shiomax/jrivermc24](https://hub.docker.com/r/shiomax/jrivermc24) |

# Screenshots

![screenshot](https://uploads.shio.at/jrivermc/screenshot.png)

# How to use this

**Bridge networking license activation** With ```network_mode: "bridge"``` (that's the default value). You will have to specify a custom mac address
for every container you create and keep it the same mac address on that container. Because docker will create it's own virtual network and it will
re-create said network every time you take down the container (at least every single up or downgrade). This makes your license activation
expier and you will have to re-activate it. Specifying a mac address fixes this. *This is not required when using your host's network.*

### **Docker Compose**

I'd recommend installing [docker-compose](https://docs.docker.com/compose/), because it's just a more sane way to manage creating docker containers.
You'll create a ```docker-compose.yml``` config file that's roughly similar to the wall of text you'd otherwise have to type into the terminal.

Bridge networking (default)

```yml
version: '3'
services:
  jrivermc26:
    image: shiomax/jrivermc26
    restart: always
    container_name: jrivermc26
    mac_address: ed:e8:60:2d:65:c1
    ports:
      - "5800:5800"
      - "5900:5900"
      - "52100:52100"
      - "52101:52101"
      - "52199:52199"
      - "1900:1900/udp"
    environment:
      - VNC_PASSWORD=12345
    volumes:
        - /path/to/config:/config:rw
        - /path/to/music:/data/music:rw
```

Or use host network.

```yml
version: '3'
services:
  jrivermc26:
    image: shiomax/jrivermc26
    restart: always
    container_name: jrivermc26
    network_mode: host
    environment:
      - VNC_PASSWORD=12345
    volumes:
        - /path/to/config:/config:rw
        - /path/to/music:/data/music:rw
```

To run the container ```docker-compose up -d```

Updating the container
- ```docker-compose pull``` to pull the latest image as specified in the compose file
- ```docker-compose up -d``` to create the container and detach from the screen
- ```docker-compose down``` to stop and remove the container (does not mean your data is gone, assuming you have mounted your /config volume)
- ```docker image prune``` to cleanup unused images

### **Without Docker Compose**

Bridge networking (default)

```sh
docker run -d \
    --name=jrivermc26 \
    --net=bridge \
    --restart=always \
    --mac-address=ed:e8:60:2d:65:c1 \
    -p 5800:5800 \
    -p 5900:5900 \
    -p 52199:52199 \
    -p 52101:52101 \
    -p 52100:52100 \
    -p 1900:1900/udp \
    -v config:/config:rw \
    -v /path/to/music:/data/music:rw \
    -e VNC_PASSWORD=12345 \
    shiomax/jrivermc26
```

Or use host network.

```sh
docker run -d \
    --name=jrivermc26 \
    --net=host \
    --restart=always \
    -v config:/config:rw \
    -v /path/to/music:/data/music:rw \
    -e VNC_PASSWORD=12345 \
    shiomax/jrivermc26
```

Updating the container
- ```docker pull shiomax/jrivermc26:latest``` pull a new image with tag latest
- ```docker rm -f jrivermc26``` stop and remove the container with name jrivermc26
- ```docker run -d ....``` rerun the entier run command you ran to initially create the container
- ```docker image prune``` to cleanup unused images

# Image Tags


- ```shiomax/jrivermc26:latest``` 

  Using JRiver version from the 'latest' repository (this is also what you get without specifying anything)
- ```shiomax/jrivermc25:stable```
  
  Using JRiver version from the 'stable' repository, but it's otherwise the same

- Tags like ```25.0.83-latest-1``` ```25.0.50-stable-1``` ```25.0.80-latest-0 ```

  Those are copies of ```stable``` and ```latest``` tags at the time. You can use those tags if a new version of the image
causes problems to go back in time. These images are pushed once and never touched again.
The tags contain the JRiver Version (25.0.83), the JRiver repository (latest) and the image version (1).

# Volumes (Bind mounts)

Volumes:
- ```/config``` that's the home directory of the container, deffinitely mount this to an external directory, so your settings and so forth are persisted properly and your library backups will also be in there
- ```/data/music``` that's optional, I just used to keep my music in there and didn't want to mess with library restores potentionally failing on me, because the paths are different. Effectively, you can mount your music directory to anything (as many as you want). If  you don't know where to put them, just use ```/data/music```.

# Ports

Port mappings:
- Ports used by JRiver: 52100, 52101, 52199, 1900/udp
- Port for Web GUI: 5800
- Port for VNC access: 5900 (if you're only gonna use the web GUI you don't need this)

# Sound

You can map your alsa sound device ```/dev/snd``` between the host and container to enable sound playback from within the container.

- When using the docker run command ```--device /dev/snd``` as an option.

- When using docker compose add the following to your ```docker-compose.yml```.

```yml
devices:
  - "/dev/snd"
```

NOTE: this will not work prior to version 7.

# Activate JRiver using .mjr file

If the key does not work for any reason, you can use a .mjr file from [https://rover.jriver.com/cgi-bin/restore.cgi](https://rover.jriver.com/cgi-bin/restore.cgi).
Enter your license key there. Doenload the .mjr file. The file is valid for 14 days. Within those 14 days you have to activate your jriver instance.

1. Place the .mjr file into the config directory
2. Log into the container with bash ```docker exec -it <container_name> /bin/bash```
3. Activate ```exec env HOME=/config mediacenter26 /RestoreFromFile /config/<mjr_file_name>```
4. JRiver should now be activated. You can delete the .mjr file now.

# Firewall-Configuration (only relevant for host networking)

This is usually only necessary if you are using host networking. Bridge networking by default does not need any firewall configuration. As docker-proxy is already
allowed to go threw it when you install docker.

### firewalld (centos, fedora or rhel)

1. Login as root ```sudo su``` (ALL of these things need root access, or don't and prepend sudo to every command)
2. Create a file ```nano /etc/firewalld/services/jriver.xml```
```
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>jriver</short>
  <description>Ports required by JRiver MediaCenter.</description>
  <port protocol="tcp" port="5800"></port>
  <port protocol="tcp" port="52199"></port>
  <port protocol="tcp" port="52100"></port>
  <port protocol="tcp" port="52101"></port>
  <port protocol="tcp" port="5900"></port>
  <port protocol="udp" port="1900"></port>
</service>
```
3. Reload firewalld ```firewall-cmd --reload```
4. Add configuration temporarily ```firewall-cmd --add-service=jriver --zone=public```
5. Try if you can access jriver
6. If yes, make the configuration permanent 
    - ```firewall-cmd --add-service=jriver --zone=public --permanent```
    - ```firewall-cmd --reload```
7. If no, you'll have to figure that out. Since the configuration was only temporarily applied another reload will make it go away.

### ufw (ubuntu or debian *if you install ufw*)

1. Login as root ```sudo su``` or ```su``` on debain (ALL of these things need root access, otherwise prepend sudo to every command)
2. Create a file ```nano /etc/ufw/applications.d/jriver```
```
[jriver]
title=jriver
description=Ports required by JRiver MediaCenter
ports=5800,52199,52100,52101,5900/tcp|1900/udp
```
3. Update the app profile ```ufw app update jriver``` after you can check your configuration with ```ufw app info jriver```
4. Allow the app ```ufw allow jriver```

# Environment Variables

Specific to this image

| Variable       | Description                                  | Default |
|----------------|----------------------------------------------|---------|
| `MAXIMIZE_POPUPS` | When set to `1` maximizes JRiver popup Windows. | `0` |

Those are inherited from the base image [jlesage/baseimage-gui](https://github.com/jlesage/docker-baseimage-gui). Some have their default values changed.

| Variable       | Description                                  | Default |
|----------------|----------------------------------------------|---------|
|`APP_NAME`| Name of the application. | `JRiver MediaCenter 25` |
|`USER_ID`| ID of the user the application runs as.  See [User/Group IDs](https://github.com/jlesage/docker-baseimage-gui#usergroup-ids) to better understand when this should be set. | `1000` |
|`GROUP_ID`| ID of the group the application runs as.  See [User/Group IDs](https://github.com/jlesage/docker-baseimage-gui#usergroup-ids) to better understand when this should be set. | `1000` |
|`SUP_GROUP_IDS`| Comma-separated list of supplementary group IDs of the application. | (unset) |
|`UMASK`| Mask that controls how file permissions are set for newly created files. The value of the mask is in octal notation.  By default, this variable is not set and the default umask of `022` is used, meaning that newly created files are readable by everyone, but only writable by the owner. See the following online umask calculator: http://wintelguy.com/umask-calc.pl | (unset) |
|`TZ`| [TimeZone] of the container.  Timezone can also be set by mapping `/etc/localtime` between the host and the container. | `Etc/UTC` |
|`KEEP_APP_RUNNING`| When set to `1`, the application will be automatically restarted if it crashes or if user quits it. | `1` |
|`APP_NICENESS`| Priority at which the application should run.  A niceness value of -20 is the highest priority and 19 is the lowest priority.  By default, niceness is not set, meaning that the default niceness of 0 is used.  **NOTE**: A negative niceness (priority increase) requires additional permissions.  In this case, the container should be run with the docker option `--cap-add=SYS_NICE`. | (unset) |
|`TAKE_CONFIG_OWNERSHIP`| When set to `1`, owner and group of `/config` (including all its files and subfolders) are automatically set during container startup to `USER_ID` and `GROUP_ID` respectively. | `1` |
|`CLEAN_TMP_DIR`| When set to `1`, all files in the `/tmp` directory are delete during the container startup. | `1` |
|`DISPLAY_WIDTH`| Width (in pixels) of the application's window. | `1280` |
|`DISPLAY_HEIGHT`| Height (in pixels) of the application's window. | `768` |
|`SECURE_CONNECTION`| When set to `1`, an encrypted connection is used to access the application's GUI (either via web browser or VNC client).  See the [Security](https://github.com/jlesage/docker-baseimage-gui#security) section for more details. | `0` |
|`VNC_PASSWORD`| Password needed to connect to the application's GUI.  See the [VNC Password](https://github.com/jlesage/docker-baseimage-gui#vnc-password) section for more details. | (unset) |
|`X11VNC_EXTRA_OPTS`| Extra options to pass to the x11vnc server running in the Docker container.  **WARNING**: For advanced users. Do not use unless you know what you are doing. | (unset) |
|`ENABLE_CJK_FONT`| When set to `1`, open source computer font `WenQuanYi Zen Hei` is installed.  This font contains a large range of Chinese/Japanese/Korean characters. | `0` |

# Changelog

### Version 7
- Add ```audio``` group to application when mapping ```/dev/snd``` (alsa sound device) between host and the container (so the container can play sound)
- Added some metadata into builds
- More and improved tests
- New buildscript option ```--deb-url``` to be able to build images for older JRiver versions.
- amd64 builds now based on ```jlesage/baseimage-gui:debian-10-v3.5.3``` as it´s now available.

### Version 6
- Now based upon a rebuilt baseimage for debian10 shiomax/docker-baseimage-gui
- As JRiver now properly keeps sub-windows on top, xpropspy now only brings back the main Window when minimized (also works a lot better, mostly because it's just simpler to do just that)
- New buildscript with several options, also builds for arm (not in the dockerhub repo yet)
- Removed ```ENABLE_SIGNATURE``` option now just prints out version on start without any silly drawings

### Version 5
- Removed mjractivate script again (lookup "Activate JRiver using .mjr file" section on how to activate jriver via cli, if it does not activate the normal way)

### Version 4
- ~~Automatically activate JRiver with '.mjr' file~~ Removed again in V5

### Version 3
- Updated baseimage from debian9:3.5.2 to debian9:3.5.3

### Version 2
- xpropspy now uses sh instead of bash (microoptimization sh is faster, but can do less)
- ascii art on startup and environment variable ENABLE_SIGNATURE (1 by default)
- specifying debian-9-v3.5.2 instead of latest, witch is currently equivalent (to avoid breaking the image on automated builds)
- removed layer below for main jriver window from rc.xml as no longer needed (also seems to have fixed some ui gliches in the integrated webgui)
- added option MAXIMIZE_POPUPS to maximize every window that has the name "JRiver Popup Class" (0 by default)

### Version 1
- added image version